"""
Starting with a 1-indexed array of zeros and a list of operations,
for each operation add a value to each the array element between two given indices,
inclusive. Once all operations have been performed, return the maximum value in the array.
5 3
1 2 100
2 5 100
3 4 100

ans = 200
"""

from typing import List


def brute_force_max(n: int, nums: List[List[int]]) -> int:
    arr = [0] * n
    max_value = 0
    for operation in nums:
        for i in range(operation[0], operation[1]):
            arr[i - 1] += operation[2]
            if arr[i - 1] > max_value:
                max_value = arr[i - 1]
    return max_value


def optimized_max(n: int, nums: List[List[int]]) -> int:
    arr = [0] * (n + 1)
    for operation in nums:
        arr[operation[0] - 1] += operation[2]
        arr[operation[1]] -= operation[2]
    max_value = 0
    curr_max = 0
    for i in arr:
        curr_max += i
        max_value = max(max_value, curr_max)

    return max_value
