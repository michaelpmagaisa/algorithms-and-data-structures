"""
Mark and Jane are very happy after having their first child.
Their son loves toys, so Mark wants to buy some. There are a
number of different toys lying in front of him, tagged with
their prices. Mark has only a certain amount to spend, and he
wants to maximize the number of toys he buys with this money.
Given a list of toy prices and an amount to spend, determine
the maximum number of gifts he can buy.

Note Each toy can be purchased only once.
"""


def maximumToys(prices, k):
    if not prices or k == 0:
        return 0
    if len(prices) == 1:
        if prices[0] <= k:
            return 1
        return 0
    prices.sort()
    total_cost = prices[1] + prices[0]
    start_window = 0
    curr_window = 2
    max_items = 0
    while curr_window < len(prices):
        if total_cost + prices[curr_window] <= k:
            total_cost += prices[curr_window]
            curr_window += 1
        else:
            total_cost -= prices[start_window]
            max_items = curr_window - start_window
            if total_cost + prices[curr_window] <= k:
                total_cost += prices[curr_window]
                curr_window += 1
            else:
                return max_items
            start_window += 1
    return max(curr_window - start_window, max_items)
