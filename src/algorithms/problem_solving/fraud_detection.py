"""
Given the spending history of a user over n days, find how many times the user
    will receive activity notifications in this period.
A user recieves notifications if the amount spent on a particular day is
    greater than or equal to to the median he has spent over the last few days
    given as the parameter look_back_days.
"""


def median(nums):
    nums.sort()
    mid_point = len(nums) // 2
    if len(nums) % 2 == 1:
        return nums[mid_point]
    return (nums[mid_point - 1] + nums[mid_point]) / 2


def activityNotifications(expenditure, look_back_days):
    notifications = 0
    start = 0
    days = []
    for i in range(look_back_days, len(expenditure) - 1):
        prior_days = expenditure[start:i]
        days.append(prior_days)
        current_day = i
        if expenditure[current_day] >= 2 * median(prior_days):
            notifications += 1
        start += 1
    return notifications
