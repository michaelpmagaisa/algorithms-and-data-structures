"""
You are given an unordered array consisting of consecutive integers
[1, 2, 3, ..., n] without any duplicates. You are allowed to swap
any two elements. Find the minimum number of swaps required to sort
the array in ascending order.
Example: [7, 1, 3, 2, 4, 5, 6] -> [1, 2, 3, 4, 5, 6, 7] needs 5 swaps
"""


def min_swaps(nums):
    swaps = 0
    i = 0
    while i < (len(nums)):
        if nums[i] != i + 1:
            temp = nums[i]
            nums[i] = nums[temp - 1]
            nums[temp - 1] = temp
            swaps += 1
        else:
            i += 1
    return swaps
