from dataclasses import dataclass
from typing import List


@dataclass
class PartitionResult:
    nums: List[int]
    swaps: int
    swaps_including_pivot_swap: int
    pivot_index: int


def partition_array(nums: List[int], start: int, end: int) -> PartitionResult:
    pivot_value = nums[end]
    # elements to the left of the pivot should be less than pivot
    # elements to the right of pivot should be greater than pivot
    # if two elements are misplaced then, just swap them
    left = start
    right = start
    swaps = 0
    while right < end:
        if nums[right] < pivot_value:
            temp = nums[right]
            nums[right] = nums[left]
            nums[left] = temp
            left += 1
            swaps += 1
        right += 1
    # now put the pivot in its new sorted position
    nums[end] = nums[left]
    nums[left] = pivot_value

    return PartitionResult(
        nums=nums, swaps=swaps, swaps_including_pivot_swap=swaps + 1, pivot_index=left
    )


def quick_sort(nums, start, end):
    if start < end:
        partition = partition_array(nums, start, end)
        # quick sort left side of pivot
        quick_sort(partition.nums, start, partition.pivot_index - 1)
        # quick sort right side of pivot
        quick_sort(partition.nums, partition.pivot_index + 1, end)
    return nums
