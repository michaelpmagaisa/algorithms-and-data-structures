def insertion_sort(nums):
    """
    Start with a sorted list of 1, iterate through entire list
    while determining where in the sorted list the number should
    be inserted.
    E.G Like how you sort cards, when you are being handed out cards in a card game
    :param nums:
    :return:
    """
    sorted_pointer = 1
    while sorted_pointer < len(nums):
        # store current value we have, and move all elements 1 step to the right
        #   until the index of this value.
        value = nums[sorted_pointer]

        # move all elements that are greater than the value we have 1 step to the right.
        i = sorted_pointer - 1
        while i >= 0 and nums[i] > value:
            nums[i + 1] = nums[i]
            i -= 1
        nums[i + 1] = value
        sorted_pointer += 1
    return nums
