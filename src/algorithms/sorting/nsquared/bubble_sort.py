def bubble_sort(nums):
    """
    Bubble up the largest number to the end of the list,
        thus the largest is sorted into place.
    Repeat.
    :param nums:
    :return: nums
    """
    sorted_pointer = len(nums) - 1
    while sorted_pointer > 0:
        for i in range(sorted_pointer):
            if nums[i] > nums[i + 1]:
                temp = nums[i]
                nums[i] = nums[i + 1]
                nums[i + 1] = temp
        sorted_pointer -= 1
    return nums
