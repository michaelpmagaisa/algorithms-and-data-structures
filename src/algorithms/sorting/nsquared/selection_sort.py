def selection_sort(nums):
    """
    Select the smallest number in each iteration, and swap it's current possition
    with the first position in the unsorted part of array
    :param nums:
    :return: nums
    """
    sorted_pointer = 0
    while sorted_pointer < len(nums) - 1:
        smallest = nums[sorted_pointer]
        index_of_smallest = sorted_pointer
        for i in range(sorted_pointer + 1, len(nums)):
            if nums[i] < smallest:
                smallest = nums[i]
                index_of_smallest = i
        temp = nums[sorted_pointer]
        nums[sorted_pointer] = nums[index_of_smallest]
        nums[index_of_smallest] = temp
        sorted_pointer += 1
    return nums
