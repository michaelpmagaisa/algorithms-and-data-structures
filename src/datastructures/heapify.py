"""
    HeapDataStructure: Encapsulated heap data structure
    Author: Michael Magaisa
"""
from dataclasses import dataclass
from typing import List, Union


@dataclass
class HeapDataStructure:
    nums: List[Union[int, float, str]]

    @property
    def max_heap(self) -> "HeapDataStructure":
        return self.heapify(nums=self.nums, max_heapify=True)

    @property
    def min_heap(self) -> "HeapDataStructure":
        return self.heapify(nums=self.nums, max_heapify=False)

    @property
    def peek(self) -> Union[int, str, float]:
        return self.nums[0]

    def insert(self, element: Union[int, float, str], max_heapify: bool) -> None:
        if not self.nums:
            self.nums = [element]
        else:
            self.nums.append(element)
            if max_heapify:
                self.max_heap
            else:
                self.min_heap

    def delete(self, element) -> None:
        max_heapify: bool = True if self.nums.index(max(self.nums)) == 0 else False
        index_of_element = self.nums.index(element)
        if index_of_element == len(self.nums) - 1:
            # if it is already the last element, just delete, no need to re-heapify
            self.nums.pop()
        else:
            # swap the positions of the element you want to delete and move it to the
            #   position of the last element( last leaf node) of the heap.
            self.nums[-1], self.nums[index_of_element] = (
                self.nums[index_of_element],
                self.nums[-1],
            )

            # now that the element is our last leaf in the heap, delete it.
            self.nums.pop()

            # now heapify again since the order has been disrupted by delition
            if max_heapify:
                self.max_heap
            else:
                self.min_heap

    @classmethod
    def heapify(cls, nums, max_heapify: bool = False):
        """
        Algorithm:
            - chose a none leaf node at n/2 - 1 as starting point
            - iterate from the index of this node, back to 0.
            - for each iteration, compare if the value of node is
                bigger than all its child nodes, if node, swap value
                of current node with the child node that is biggest
                if max heaap, other wise node should be smaller than
                all it's child nodes if creating min heap
            - repeat until fully heapified
        :param nums:
        :param max_heapify:
        :return: HeapDataStructure
        """
        size_of_array = len(nums)
        overall_starting_index = (size_of_array // 2) - 1
        while overall_starting_index >= 0:
            current_starting_index = overall_starting_index
            while current_starting_index >= 0:
                index_of_largest_element = current_starting_index
                left_child = (2 * current_starting_index) + 1
                right_child = left_child + 1

                index_of_largest_element = (
                    max_heapify_indexer(
                        left_child,
                        right_child,
                        index_of_largest_element,
                        size_of_array,
                        nums,
                    )
                    if max_heapify
                    else min_heapify_indexer(
                        left_child,
                        right_child,
                        index_of_largest_element,
                        size_of_array,
                        nums,
                    )
                )

                if index_of_largest_element != current_starting_index:
                    nums[index_of_largest_element], nums[current_starting_index] = (
                        nums[current_starting_index],
                        nums[index_of_largest_element],
                    )

                current_starting_index -= 1
            overall_starting_index -= 1

        return HeapDataStructure(nums)


def min_heapify_indexer(
    left_child, right_child, index_of_largest_element, size_of_array, nums
):
    if left_child < size_of_array and nums[left_child] < nums[index_of_largest_element]:
        index_of_largest_element = left_child

    if (
        right_child < size_of_array
        and nums[right_child] < nums[index_of_largest_element]
    ):
        index_of_largest_element = right_child

    return index_of_largest_element


def max_heapify_indexer(
    left_child, right_child, index_of_largest_element, size_of_array, nums
):
    if left_child < size_of_array and nums[left_child] > nums[index_of_largest_element]:
        index_of_largest_element = left_child

    if (
        right_child < size_of_array
        and nums[right_child] > nums[index_of_largest_element]
    ):
        index_of_largest_element = right_child

    return index_of_largest_element
