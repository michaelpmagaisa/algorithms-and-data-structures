from src.datastructures.heapify import HeapDataStructure


def test_heap_data_structure():
    # binary_tree = make_binary_tree
    binary_tree_for_max_heap = [3, 9, 2, 1, 4, 5]
    binary_tree_for_min_heap = [3, 9, 2, 1, 4, 5]
    # strings_binary_tree = ["mike", "sarah", "laura", "jane", "ruth", "ane", "zeke"]
    expected_max_heap = [9, 4, 5, 1, 3, 2]
    expected_min_heap = [1, 3, 2, 9, 4, 5]
    # expected_strings_min_heap = ["ane", "jane", "laura", "sarah", "ruth", "mike", "zeke"]

    # test via class method
    max_heap = HeapDataStructure.heapify(binary_tree_for_max_heap, max_heapify=True)
    min_heap = HeapDataStructure.heapify(binary_tree_for_min_heap, max_heapify=False)
    # min_strings_heap = heapify(strings_binary_tree, size_of_array, starting_index, max_heapify=False)

    assert max_heap.nums == expected_max_heap
    assert min_heap.nums == expected_min_heap
    # assert min_strings_heap == expected_strings_min_heap

    # test via properties
    binary_tree_for_max_heap = [3, 9, 2, 1, 4, 5]
    binary_tree_for_min_heap = [3, 9, 2, 1, 4, 5]
    heap_1 = HeapDataStructure(nums=binary_tree_for_max_heap)
    heap_2 = HeapDataStructure(nums=binary_tree_for_min_heap)

    assert heap_1.max_heap.nums == expected_max_heap
    assert heap_2.min_heap.nums == expected_min_heap


def test_heap_insert_and_delete():
    binary_tree_for_max_heap = [3, 9, 2, 1, 4, 5]
    element_to_insert = 7
    expected_after_insert = [9, 4, 7, 1, 3, 5, 2]
    heap = HeapDataStructure(nums=binary_tree_for_max_heap)
    heap.insert(element=element_to_insert, max_heapify=True)
    assert heap.nums == expected_after_insert

    # delete 7 from the heap
    heap.delete(7)
    expected_max_heap_after_delete = [9, 4, 5, 1, 3, 2]
    assert heap.nums == expected_max_heap_after_delete

    # delete last element
    heap.delete(2)
    assert heap.nums == expected_max_heap_after_delete[:-1]


def test_heap_creation_via_insert():
    max_heap = HeapDataStructure(nums=[])
    binary_tree_for_max_heap = [3, 9, 2, 1, 4, 5]
    expected_max_heap = [9, 4, 5, 1, 3, 2]
    for element in binary_tree_for_max_heap:
        max_heap.insert(element, max_heapify=True)

    assert max_heap.nums == expected_max_heap


def test_peek():
    binary_tree_for_max_heap = [3, 9, 2, 1, 4, 5]
    heap = HeapDataStructure(nums=binary_tree_for_max_heap)
    heap.max_heap
    assert heap.peek == max(binary_tree_for_max_heap)
