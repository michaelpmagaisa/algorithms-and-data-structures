from src.algorithms.sorting.nsquared.bubble_sort import bubble_sort
from src.algorithms.sorting.nsquared.selection_sort import selection_sort
from src.algorithms.sorting.nsquared.insertion_sort import insertion_sort
import pytest


def parameters():
    """
    These are test cases.
    Add new test case input and output as well as id here
    :return: list of pytest.param args
    """
    return [
        pytest.param([5, 2, 9, 1, 7, 0, 3], [0, 1, 2, 3, 5, 7, 9], id="random order"),
        pytest.param([3, 5, 1], [1, 3, 5], id="small big small: very small list"),
        pytest.param(
            [3, 5, 1, 7, 2, 9, -1],
            [-1, 1, 2, 3, 5, 7, 9],
            id="small, big, small: longer list",
        ),
        pytest.param([5, 4, 3, 2, 1], [1, 2, 3, 4, 5], id="reverse sorted"),
    ]


@pytest.mark.parametrize("unsorted_list, sorted_list", parameters())
def test_bubble_sort(unsorted_list, sorted_list):
    assert bubble_sort(unsorted_list) == sorted_list


@pytest.mark.parametrize("unsorted_list, sorted_list", parameters())
def test_selection_sort(unsorted_list, sorted_list):
    assert selection_sort(unsorted_list) == sorted_list


@pytest.mark.parametrize("unsorted_list, sorted_list", parameters())
def test_insertion_sort(unsorted_list, sorted_list):
    assert insertion_sort(unsorted_list) == sorted_list


def test_maximum_values_after_operations():
    from src.algorithms.problem_solving.maximum_array_value import (
        brute_force_max,
        optimized_max,
    )

    nums = [[1, 2, 100], [2, 5, 100], [3, 4, 100]]
    assert brute_force_max(n=5, nums=nums) == 200

    nums2 = [[1, 5, 3], [4, 8, 7], [6, 9, 1]]
    assert brute_force_max(10, nums2) == 10

    assert optimized_max(5, nums) == 200
    assert optimized_max(10, nums2) == 10


def test_min_swaps():
    from src.algorithms.sorting.miswaps import min_swaps

    assert min_swaps([7, 1, 3, 2, 4, 5, 6]) == 5


@pytest.mark.xfail(reason="BUG: Please debug this later!!!!")
def test_maximum_toys():
    from src.algorithms.problem_solving.maximum_toys import maximumToys

    nums = (
        "52952808 39586066 70403274 84627963 46636834 13906132 18138605 "
        "22451014 71348257 91939176 17451226 31356009 15266983 33392541 "
        "37992362 55743111 55380991 48022854 54843595 440"
    )
    nums = nums.split()
    nums = list(map(int, nums))
    assert maximumToys(nums, 100000) == 1


@pytest.mark.parametrize("unsorted_list, sorted_list", parameters())
def test_quick_sort(unsorted_list, sorted_list):
    from src.algorithms.sorting.nsquared.quick_sort import partition_array, quick_sort

    # first check if the partion algorithm works
    nums = [12, 9, 7, 15, 10]
    expected_partition_result_after_one_run = [9, 7, 10, 15, 12]
    expected_swaps = 2
    partition_results = partition_array(nums, 0, len(nums) - 1)
    assert partition_results.nums == expected_partition_result_after_one_run
    assert partition_results.swaps == expected_swaps
    assert partition_results.swaps_including_pivot_swap == expected_swaps + 1

    # then check if the quick sort algorithm works
    assert quick_sort(unsorted_list, 0, len(unsorted_list) - 1) == sorted_list


def test_fraud_detection():
    from src.algorithms.problem_solving.fraud_detection import (
        median,
        activityNotifications,
    )

    # first test that the median finder works properly
    nums = [[3, 4, 1, 7, 9, 2], [3, 4, 5], [9, 7, 6, 4], [4, 4, 1, 7, 9, 2]]
    answers = [(3 + 4) / 2, 4, (7 + 6) / 2, 4]
    median(nums[1])
    for n, a in zip(nums, answers):
        assert median(n) == a

    days = 5
    sample = [2, 3, 4, 2, 3, 6, 8, 4]
    ans = [3, 3, 4, 4]
    for i in range(len(sample) - days):
        sub_sample = sample[i : days + i]
        assert median(sub_sample) == ans[i]

    # check if notification calculated are correct
    assert activityNotifications(sample, days) == 2
